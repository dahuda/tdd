package com.learning;

import org.junit.Before;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Göksel Ünal <kunal@ebay.com>
 */
public class SaleByBarCodeTest {

    private Sale sale;
    private Display display;

    @Before
    public void setUp() {
        Map<String, Money> moneyMap = new HashMap<String, Money>();
        moneyMap.put("1234", Money.cents(600));
        moneyMap.put("4567", Money.cents(400));

        Catalog catalog = new Catalog(moneyMap);
        display = new Display();
        sale = new Sale(display, catalog);
    }

    @Test
    public void sellItem1234Test() {
        sale.onBarcode("1234");
        assertEquals("TRL 6,00", display.getText());
    }

    @Test
    public void sellItem4567Test() {
        sale.onBarcode("4567");
        assertEquals("TRL 4,00", display.getText());
    }

    @Test
    public void emptyBarcodeTest() {
        sale.onBarcode("");
        assertEquals("Error: Empty barcode", display.getText());
    }

    @Test
    public void invalidBarcodeTest() {
        sale.onBarcode("9999");
        assertEquals("Invalid barcode 9999", display.getText());
    }

    @Test
    public void sellItem1234Item4567Test() {
        sale.onBarcode("1234");
        sale.onBarcode("4567");
        assertEquals("TRL 10,00", display.getText());
    }
}

class Sale implements ReceivesScannerInput {

    Display display;
    Money currentMoney = Money.cents(0);
    Catalog catalog;

    public Sale(Display display, Catalog catalog){
        this.display = display;
        this.catalog = catalog;
    }

    @Override
    public void onBarcode(String barcode) {

        if(barcode.equals("")) {
            display.setText("Error: Empty barcode");
            return;
        }

        if (catalog.hasItem(barcode)) {
            currentMoney = currentMoney.plus(catalog.moneyForBarcode(barcode));
            display.setText(currentMoney.toFormatted());
        } else {
            display.setText("Invalid barcode "+barcode);
        }

    }

    @Override
    public void close() {

    }

}

class Catalog {

    private Map<String, Money> moneyMap;

    public Catalog(Map<String, Money> moneyMap) {
        this.moneyMap = moneyMap;
    }

    public boolean hasItem(String barcode) {
        return (moneyMap.containsKey(barcode));
    }

    public Money moneyForBarcode(String barcode) {
        return moneyMap.get(barcode);
    }

}

class Display {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

class Money {

    int currentCents;
    public static Money cents(int cents) {
        Money money = new Money();
        money.currentCents = cents;
        return money;
    }
    public String toFormatted() {
        return new DecimalFormat("TRL #0,00").format(currentCents).toString();
    }

    public Money plus(Money money) {
        int summedCents = this.currentCents + money.getCurrentCents();
        return Money.cents(summedCents);
    }

    int getCurrentCents() {
        return currentCents;
    }
}
