package com.learning;

/**
 * @author Göksel Ünal <kunal@ebay.com>
 */
public interface ReceivesScannerInput {

    void onBarcode(String barcode);

    void close();
}
